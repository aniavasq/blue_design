<?php include 'base.php' ?>

<?php startblock('title') ?>Home<?php endblock() ?>

<?php startblock('content') ?>
    <div id="myCarousel" class="carousel slide row" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="static/img/slide0.png" alt="Hogar" style="width:100%;">
            </div>

            <div class="item">
                <img src="static/img/slide1.png" alt="COmedores" style="width:100%;">
            </div>
        
            <div class="item">
                <img src="static/img/slide2.png" alt="Oficina" style="width:100%;">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<?php endblock() ?>