<?php include 'base.php' ?>

<?php startblock('title') ?>Mantenimiento<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">MANTENIMIENTO</h2>
<div class="row">
    <div class="col-sm-12 col-lg-6">
        <img src="static/img/g2178.png" alt="Tips e Ideas" style="width: 100%;">
    </div>
    <div class="col-sm-12 col-lg-6">
        <p>Nos ocupamos de mantener sus anaqueles de cocina y closets en condicion adecuada y funcional, y de los procesos para lograrlo.</p>
        <br>
        <p>Incluyen acciones de inspeccion, comprobaciones, clasificacion, reparacion etc.</p>
    </div>
</div>
<?php endblock() ?>