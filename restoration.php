<?php include 'base.php' ?>

<?php startblock('title') ?>Restauración<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">RESTAURACIÓN</h2>
<div class="row">
    <div class="col-sm-12 col-lg-6">
        <p>Involucra pelado, pintura, reemplazar madera y material dañada o faltante, hacer funcionar herrajes existentes o reemplazarlos.</p>
        <br>
        <p>Adicionalmente se restaura la pintura del mueble, dándole color y el acabado con el esilo deseado.</p>
        <br>
        <p>Si hay tapicería, volvemos a realizar esta labor cambiando todos los materiales internos y así dar nueva vida a los muebles.</p>
    </div>
    <div class="col-sm-12 col-lg-6">
        <img src="static/img/g2166.png" alt="Tips e Ideas" style="width: 100%;">
    </div>
</div>
<?php endblock() ?>