<div class="footer row">
    <hr>
    <p style="text-align: center;">
        <span class="text-muted">&copy;</span> <span class="text-warning"><?php echo SITE_NAME; ?></span> <span class="text-muted"><?php echo date("Y"); ?></span>
    </p>
    <p>
        <ul class="list-inline" id="footer-description" style="text-align: center;">
            <li><a href="#"><i class="fa fa-sitemap" aria-hidden="true"></i> Sitemap</a><span class="footer-bullet"> &emsp;&bull;&emsp;</span></li>
            <li><a href="mailto:info@bluedesign.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@bluedesign.com</a><span class="footer-bullet"> &emsp;&bull;&emsp;</span></li>   
            <li><a href="tel:+59342880308"><i class="fa fa-mobile" aria-hidden="true"></i> +593 (42) 880-308</a><span class="footer-bullet"> &emsp;&bull;&emsp;</span></li>
            <li><a href="#"><i class="fa fa-location-arrow" aria-hidden="true"></i> Guayaquil, Dirección de ejemplo</a></li>  
        </ul>
    </p>
</div>