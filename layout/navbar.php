<nav class="navbar navbar-inverse row" role="navigation">  
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<a class="navbar-brand" href="#"><?php echo SITE_NAME;?></a>-->
        </div>
        <div class="collapse navbar-collapse" id="centerednav">
            <ul class="nav navbar-nav">
                <li <?php if ($isHome): ?>class="active"<?php endif ?>><a href="<?php echo ROOT_URL; ?>">HOME</a></li>
                <li <?php if ($isAboutUs): ?>class="active"<?php endif ?>><a href="about.php">ACERCA DE</a></li>
                <!--
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">PRODUCTOS <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">MATERIAL</a></li>
                        <li><a href="furniture.php">MUEBLERÍA</a></li>
                        <li><a href="#">MUEBLERÍA DEL HOGAR</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">UBICACIONES <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="showrooms.php">SHOWROOMS</a></li>
                        <li><a href="#">DISTRIBUIDORES</a></li>
                    </ul>
                </li>
                <li><a href="#about">GALERÍA</a></li>
                -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">MUEBLES <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-header">SALAS</li>
                        <li><a href="mesas_de_centro_esquineros.php">Mesas de Centro y Esquineros</a></li>
                        <li><a href="consolas.php">Consolas</a></li>
                        <li><a href="centros_de_entretenimiento.php">Centros de Entretenimiento</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">COMEDORES</li>
                        <li><a href="mesas.php">Mesas</a></li>
                        <li><a href="aparadores.php">Aparadores</a></li>
                        <!--<li><a href="#">Bares</a></li>-->
                        <li class="divider"></li>
                        <li class="dropdown-header">DORMITORIOS</li>
                        <li><a href="veladores.php">Veladores</a></li>
                        <li><a href="espejos.php">Espejos</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">SERVICIOS <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="restoration.php">RESTAURACIÓN</a></li>
                        <li><a href="maintenance.php">MANTENIMIENTO</a></li>
                    </ul>
                </li>
                <li><a href="contact.php">CONTACTO</a></li>
                <li><a href="tips.php">TIPS E IDEAS</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>