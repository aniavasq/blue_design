<?php include 'base.php' ?>
<?php include 'data_connector.php' ?>

<?php

$raw_data = $raw_data['furniture'];
// use get variable to paging number
$item_index = !isset($_GET['item']) ? -1 : $_GET['item'];
try {
    if (isset($raw_data[$item_index])) {
        $element = $raw_data[$item_index];
    } else {
        http_response_code(404);
        echo "<h1>404 File not found</h1>";
        die();
    }
} catch (Exception $e) {
    http_response_code(404);
    echo "<h1>404 File not found</h1>";
    die();
}

?>

<?php startblock('title') ?>Mueblería<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title"><?php echo strtoupper($element['name']); ?></h2>
<div class="row">
    <div class="col-sm-12 col-md-7">
        <img src="<?php echo $element['img']; ?>" alt="<?php echo $element['name']; ?>" style="width: 100%;">
    </div>
    <div class="col-sm-12 col-md-4">
        <p style="text-align: justify; text-justify: inter-word;"><?php echo $element['description']; ?></p>
    </div>
</div>
<?php endblock() ?>