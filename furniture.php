<?php include 'base.php' ?>
<?php include 'data_connector.php' ?>

<?php

$raw_data = $raw_data['furniture'];
// use get variable to paging number
$page = !isset($_GET['page']) ? 1 : $_GET['page'];
$limit = 9; // nine rows per page
$offset = ($page - 1) * $limit; // offset
$total_items = count($raw_data); // total items
$total_pages = ceil($total_items / $limit);
$data = array_splice($raw_data, $offset, $limit); // splice them according to offset and limit

?>

<?php startblock('header-style') ?>
<style type="text/css">
    .gallery-img {
        height: 300px;
        width: 100%;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .gallery-element {
        background-color: rgb(256, 256, 256);
        width: 100%;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .bottom-pagination {
        text-align: center;
    }

    .bottom-pagination > div > ul > li > a {
        border: solid white 1px;
        margin-left: 5px;
        margin-right: 5px;
    }

    .footer-active {
        color: #d1b585 !important;
    }
</style>
<?php endblock() ?>

<?php startblock('title') ?>Mueblería<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">MUEBLERÍA</h2>
<div class="row">
    <?php foreach($data as $key => $value): ?>
        <div class="col-sm-12 col-md-4">
            <a href="furniture_item.php?item=<?php echo $value['id']; ?>" class="gallery-element">
                <div class="gallery-img" style="background: url('<?php echo $value['img']; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<!-- print links -->
<div class="row bottom-pagination">
    <div class="col-sm-12">
        <ul class="pagination">
        <?php if($page != 1): ?>
            <li><a href="furniture.php?page=1" style="border: none;">&#10094;</a></li>
        <?php endif ?>
        <?php for($x = 1; $x <= $total_pages; $x++): ?>
            <li><a href="furniture.php?page=<?php echo $x; ?>" style="border-radius: 50%;" <?php if($page == $x): ?>class="footer-active"<?php endif ?>><?php echo $x; ?></a></li>
        <?php endfor; ?>
        <?php if($page != $total_pages): ?>
            <li><a href="furniture.php?page=<?php echo $total_pages; ?>" style="border: none;">&#10095;</a></li>
        <?php endif ?>
        </ul>
    </div>
</div>
<?php endblock() ?>