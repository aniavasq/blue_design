(function($, document, window, viewport){

    var responsiveList = function() {

        if( viewport.is("<=sm") ) {
            $('#footer-description').attr('class', 'list-unstyled');
            $('.footer-bullet').hide();
        }

        if( viewport.is(">=md") ) {
            $('#footer-description').attr('class', 'list-inline');
            $('.footer-bullet').show();
        }
    }

    // Executes once whole document has been loaded
    $(document).ready(function() {
        responsiveList();
    });

    $(window).resize(
        viewport.changed(function(){
            responsiveList();
        })
    );

})(jQuery, document, window, ResponsiveBootstrapToolkit);