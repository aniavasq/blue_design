<?php include 'base.php' ?>

<?php startblock('title') ?>Tips e Ideas<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">TIPS E IDEAS</h2>
<div class="row">
    <div class="col-sm-12 col-lg-6">
        <p>Ambientes en los que la mezcla de lo natural con lo sofisicado sean la pauta. Materiales como la piedra natural están de moda en todos los colores: blanco, negro, verde, rosa... También los metales como el latón o el cobre maificados por maderas en su estado más natural. Un ambiente sofisicado puede a la vez transmiir frescura. Lo importante es buscar el contraste, de esilos, de colores, de texturas.</p>
    </div>
    <div class="col-sm-12 col-lg-6">
        <img src="static/img/g2202.png" alt="Tips e Ideas" style="width: 100%;">
    </div>
    <div class="col-sm-12">
        <br>
        <p>Las telas siempre de tejidos naturales, sin brillos, como linos, terciopelos de algodón o los nuevos tejidos procedentes de plantas, como el bambú, son siempre un acierto. Las mezclas que más me gustan son aquellas que permiten a cada pieza o esilo arquitectónico mantener su protagonismo a la vez que funcionan juntas.</p>
    </div>
</div>
<?php endblock() ?>