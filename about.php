<?php include 'base.php' ?>

<?php startblock('title') ?>La Compañía<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">LA COMPAÑÍA</h2>
<div class="row">
    <div class="col-sm-12 about">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3>MISIÓN</h3>
                <p>Ofrecer productos y servicios de calidad en el campo inmobiliario capaz de saisfacer con las necesidades del cliente, entregándole muebles con acabados de calidad y de la más fina elaboración.</p>
            </div>
            <div class="col-sm-12 col-md-12">
                <h3>VISIÓN</h3>
                <p>Construir un vínculo de confianza y profesionalismo entre el cliente y la compañía con fin de lograr ser una empresa que represente el esilo y elegancia que cada hogar desee dentro de sus cuartos preferidos.</p>
            </div>
            <div class="col-sm-12 col-md-12">
                <h3>OBJETIVO</h3>
                <p>Implementar un nuevo esilo de mueblería dentro del país, con acabados de calidad original y mayor resistencia ante cualquier percance que se le presente para que los clientes sientan una saisfacción del trabajo realizado por la empresa.</p>
            </div>
        </div>
    </div>
</div>
<?php endblock() ?>