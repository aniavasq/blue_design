<?php include 'base.php' ?>

<?php startblock('title') ?>Contacto<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">CONTACTO</h2>
<div class="row">
    <div class="col-sm-12 col-lg-6 col-lg-offset-3">
        <form>
            <fieldset>
                <div class="form-group">
                    <label for="inputName" class="col-sm-12 control-label">Nombre Completo</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="inputName" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputCompany" class="col-sm-12 control-label"><br>Compañía</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="inputCompany" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-12 control-label"><br>Email</label>
                    <div class="col-sm-12">
                        <input type="email" class="form-control" id="inputEmail" placeholder="email@ejemplo.com">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPhone" class="col-sm-12 control-label"><br>Teléfono</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="inputPhone">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputSubject" class="col-sm-12 control-label"><br>Asunto</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="inputSubject" style="text-transform: capitalize;">
                    </div>
                </div>
                <div class="form-group">
                    <label for="textAreaMessage" class="col-sm-12 control-label"><br>Su mensaje</label>
                    <div class="col-sm-12">
                        <textarea class="form-control" rows="5" id="textAreaMessage"></textarea>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Checkbox
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="textArea" class="col-lg-2 control-label">Textarea</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" id="textArea"></textarea>
                        <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Radios</label>
                    <div class="col-lg-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                                Option one is this
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="select" class="col-lg-2 control-label">Selects</label>
                    <div class="col-lg-10">
                        <select class="form-control" id="select">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                        <br>
                        <select multiple="" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>-->

                <div class="form-group">
                    <div class="col-sm-12">
                        <br>
                        <button type="submit" class="btn btn-primary">ENVIAR</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<?php endblock() ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#inputPhone").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
</script>