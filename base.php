
<?php
define("BACKGROUND_COLOR", "#000a18");
define("SITE_NAME", "Blue Design");
define("SITE_DESCRIPTION", "Blue Design Web Site");
define('APP_BASE_PATH', dirname(__FILE__));
define('APP_LIBS_PATH', APP_BASE_PATH . '/libs');
define('APP_LAYOUT_PATH', APP_BASE_PATH . '/layout');
define('APP_ASSETS_PATH', APP_BASE_PATH . '/static/assets');
define("DEBUG", true);

if (DEBUG) {
    define('ROOT_URL', '/blue_design/');

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    define('ROOT_URL', 'http://www.bluedesign.com');
}

$isHome = false;
$isAboutUs = false;

?>
<?php require_once APP_LIBS_PATH . '/ti/ti.php' ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="<?php echo SITE_DESCRIPTION;?>">
    <meta name="author" content="[ST] Project Dev Studio">

    <!-- Custom bar for mobile-->
    <meta name="theme-color" content="<?php echo BACKGROUND_COLOR;?>">                      <!-- Chrome, Firefox OS, Opera and Vivaldi -->
    <meta name="msapplication-navbutton-color" content="<?php echo BACKGROUND_COLOR;?>">    <!-- Windows Phone -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">         <!-- iOS Safari -->

    <!-- Fav. Icon -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="static/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="static/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="static/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="static/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="static/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="static/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="static/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="static/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="static/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="static/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="static/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="<?php echo SITE_NAME;?>"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="static/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="static/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="static/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="static/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="static/favicon/mstile-310x310.png" />

    <title><?php echo SITE_NAME;?> - <?php startblock('title') ?><?php endblock() ?></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="static/css/theme.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="static/css/font-awesome.min.css">

    <!-- Custom Style -->
    <link rel="stylesheet" type="text/css" href="static/css/style.css">

    <?php startblock('header-style') ?><?php endblock() ?>
</head>
<body>
    <div class="container-fluid">
        <?php include APP_LAYOUT_PATH . '/header.php' ?>
        <?php startblock('content') ?><?php endblock() ?>
        <?php include APP_LAYOUT_PATH . '/footer.php' ?>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="static/js/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="static/js/bootstrap.min.js"></script>

    <!-- Bootstrap Toolkit -->
    <script type="text/javascript" src="static/js/bootstrap-toolkit.min.js" ></script>

    <!-- Custom JS -->
    <script type="text/javascript" src="static/js/index.js" ></script>

    <?php startblock('extra_scripts') ?><?php endblock() ?>
</body>
</html>