<?php include 'base.php' ?>

<?php startblock('title') ?>Ubicaciones<?php endblock() ?>

<?php startblock('content') ?>
<h2 class="head-title">SHOWROOMS</h2>
<div class="row">
    <div class="col-sm-12 col-md-7">
        <img src="static/img/g72.png" alt="Showroom Guayaquil" style="width: 100%;">
    </div>
    <div class="col-sm-12 col-md-4">
        <h4>GUAYAQUIL</h4>
        <p style="text-align: justify; text-justify: inter-word;">
            <ul class="fa-ul">
                <li><a href="#" style="color: rgb(256, 256, 256);"><i class="fa-li fa fa-location-arrow"></i>DIRECCIÓN DE EJEMPLO</a></li>
                <li><a href="tel:+59342880308" style="color: rgb(256, 256, 256);"><i class="fa-li fa fa-mobile"></i>+593 (42) 880-308</a></li>
                <li><i class="fa-li fa fa-clock-o"></i>LUN – VIE 10:30 AM-7:30 PM SAB 11:00 AM-5:00 PM</li>
            </ul>
        </p>
    </div>
</div>
<?php endblock() ?>